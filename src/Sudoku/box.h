#ifndef BOX_H
#define BOX_H

#include <QWidget>

class Box : public QWidget
{
	Q_OBJECT

public:

	Box(QWidget *parent, int x, int y, QChar solved, QChar unsolved);
	~Box();

	int getPositionX();
	int getPositionY();
	QSize sizeHint() const {return QSize (40, 40);}; // velikost policka v gridu

protected:
	void paintEvent(QPaintEvent *event);
	void mousePressEvent(QMouseEvent *event);

private:
	void setPosition(int x, int y);
	int pos_x;
	int pos_y;
	QPen *pen;
	QBrush *brush;
	QChar solved;
	QChar unsolved;
	void setPenAndBrush();
	bool isEditable();
	
};

#endif // BOX_H
