#ifndef GAMEBOARD_H
#define GAMEBOARD_H

#include <QWidget>
#include <QtGui/QGridLayout>
#include "box.h"

class Position{
public:


	Position();
	Position(int x, int y);
	Position &operator= (const Position & other);
	bool operator== (const Position & other) const;
	bool operator!= (const Position & other) const;
	int getX();
	int getY();
	int getOffset(int width);
	void set(int x, int y);
	Position right();
	Position left();
	Position up();
	Position down();
private:
	int x;
	int y;
};


class GameBoard : public QWidget
{
	Q_OBJECT

public:

	GameBoard(QWidget *parent);
	~GameBoard();

private:
	void createBoxes();
	QList<Box*> boxes;
	QString solved;
	QString unsolved;
	void createTrapezes();
	QGridLayout *gameBoardLayout; // grid s hracim polem
};



#endif // GAMEBOARD_H
