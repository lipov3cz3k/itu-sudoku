#include "stdafx.h"
#include "box.h"

Box::Box(QWidget *parent, int x, int y, QChar solved, QChar unsolved)
	: QWidget(parent)
{
	pen = new QPen();
	brush = new QBrush(Qt::SolidPattern);
	setPosition(x, y);
	this->solved = solved;
	this->unsolved = unsolved;
	setPenAndBrush();
}

Box::~Box(){

}

void Box::paintEvent(QPaintEvent *event){
	QPainter painter(this);
	QPainter painterNumb(this);
	
	
	painter.setBrush(*brush);
	painter.setPen(*pen);
	
	QSize s = size();
	QRect rect = QRect(QPoint (1,1), s);
	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.drawRect(rect);

	if (this->solved == this->unsolved)
		painter.drawText(rect, Qt::AlignCenter, this->unsolved);
	setMinimumSize(40, 40);
}

void Box::setPenAndBrush(){
	if (isEnabled()){
		this->pen->setColor(QColor(255, 255, 255));
		this->brush->setColor(QColor(255, 255, 255));
	}
	else {
		this->pen->setColor(QColor(0, 0, 0));
		this->brush->setColor(QColor(0, 0, 0));
	}

}

void Box::mousePressEvent(QMouseEvent *event){
	
}

bool Box::isEditable(){
	if (QString(unsolved) == "0")
		return true;
	return false;
}

void Box::setPosition(int x, int y){
	this->pos_x = x;
	this->pos_y = y;
}

int Box::getPositionX(){
	return this->pos_x;
}

int Box::getPositionY(){
	return this->pos_y;
}