#ifndef TRAPEZE_H
#define TRAPEZE_H

#include <QWidget>

class Trapeze : public QWidget
{
	Q_OBJECT

public:
	QSize sizeHint() const {return QSize (10, 10);}; // velikost policka v gridu
	Trapeze(QWidget *parent, int number);
	~Trapeze();
protected:
	void paintEvent(QPaintEvent *event);
private:
	void setPenAndBrush();
	QPen *pen;
	QBrush *brush;
	int numb;
};

#endif // TRAPEZE_H