#include "stdafx.h"
#include "gameboard.h"

GameBoard::GameBoard(QWidget *parent)
	: QWidget(parent)
{
	gameBoardLayout = new QGridLayout(this);
	gameBoardLayout->setSizeConstraint(QLayout::SetFixedSize);
	gameBoardLayout->setSpacing(0);
	
	this->unsolved = "063700024090004036000503907006050208000209000702010300608902000120600070470001690";
	  this->solved = "563798124897124536241563987916357248354289761782416359638972415129645873475831692";
	createBoxes();
}

GameBoard::~GameBoard()
{
	delete gameBoardLayout;
}

void GameBoard::createBoxes(){
	Box *tempBox;
	for (int i = 0; i < 9; i++){
		for (int j = 0; j < 9; j++){
			tempBox = new Box(this, i, j, this->solved[i*9+j], this->unsolved[i*9+j]);
			boxes.append(tempBox);
		}
	}
	for(QList<Box *>::iterator i = boxes.begin(); i!=boxes.end(); i++){
		if(*i != NULL)
			gameBoardLayout->addWidget(*i, (*i)->getPositionX(), (*i)->getPositionY());
	}
	


}


Position::Position(){
	Position::set(0, 0);
}

Position::Position(int x, int y){
	Position::set(x, y);
}

Position &Position::operator= (const Position & other){
	x = other.x;
	y = other.y;
	return *this;
}

bool Position::operator ==(const Position &other) const{
	return this->x == other.x && this->y == other.y;
}

bool Position::operator !=(const Position &other) const{
	return this->x != other.x || this->y != other.y;
}

int Position::getX(){
	return this->x;
}

int Position::getY(){
	return this->y;
}

int Position::getOffset(int width){
	return this->x*width+this->y;
}

void Position::set(int x, int y){
	this->x = x;
	this->y = y;
}

Position Position::right(){
	return Position(this->x, this->y+1); 
}

Position Position::left(){
	return Position(this->x, this->y-1); 
}

Position Position::up(){
	return Position(this->x-1, this->y); 
}

Position Position::down(){
	return Position(this->x+1, this->y); 
}
