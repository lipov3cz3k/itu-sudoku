#include "stdafx.h"
#include "sudoku.h"
#include <QtGui/QApplication>


/**
 * @brief Vstupni bod programu
 */
int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	Sudoku w(&a);
	w.show();
	return a.exec();
}
