#ifndef SUDOKU_H
#define SUDOKU_H

#include <QtGui/QMainWindow>
#include "ui_sudoku.h"
#include "gameboard.h"
#include "trapeze.h"

class Sudoku : public QMainWindow
{
	Q_OBJECT

public:
	Sudoku(QApplication *app, QWidget *parent = 0, Qt::WFlags flags = 0);
	~Sudoku();
private slots:
	void newGameSlot();
private:
	Ui::SudokuClass ui;
	QHBoxLayout *mainLayout;
	QVBoxLayout *rightLayout;
	GameBoard *gameBoard;
	void newGame();
};

#endif // SUDOKU_H
