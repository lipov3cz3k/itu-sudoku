#include "stdafx.h"
#include "trapeze.h"

Trapeze::Trapeze(QWidget *parent, int number)
	: QWidget(parent)
{
	pen = new QPen();
	brush = new QBrush();
	setPenAndBrush();
	this->numb = number;
}

Trapeze::~Trapeze(){
	
}

void Trapeze::setPenAndBrush(){
	pen->setColor(QColor(127, 0, 0));
	brush->setColor(QColor(127, 0, 0));
}

void Trapeze::paintEvent(QPaintEvent *event){
	static const QPointF points[4] = {
     QPointF(5.0, 5.0),
	 QPointF(30.0, 5.0),
     QPointF(25.0, 30.0),
     QPointF(10.0, 30.0)
	};
	QPainter painter(this);
	painter.setBrush(*brush);
	painter.setPen(*pen);
	QRect rect = QRect(QPoint (15,15), QSize(40, 40));
	//painter.setCompositionMode(QPainter::CompositionMode_SourceAtop);

	painter.setRenderHint(QPainter::Antialiasing, true);
	
	painter.drawPolygon(points, 4, Qt::WindingFill);
	painter.drawText(rect, tr("%1").arg(numb));

}