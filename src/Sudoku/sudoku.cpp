#include "stdafx.h"
#include "sudoku.h"

Sudoku::Sudoku(QApplication *app, QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);
	this->setWindowTitle(QString(tr("Sudoku")));
	this->mainLayout = new QHBoxLayout(ui.centralWidget);
	this->gameBoard = NULL;
	this->rightLayout = NULL;
	connect(ui.actionNew_game, SIGNAL(triggered()), this, SLOT(newGameSlot()));
	connect(ui.actionExit, SIGNAL(triggered()), app, SLOT(quit()));
	newGame();
}

Sudoku::~Sudoku()
{
	if (this->rightLayout != NULL)
		delete this->rightLayout;
	if (this->gameBoard != NULL)
		delete this->gameBoard;
}

void Sudoku::newGame(){

	if (this->rightLayout != NULL)
		delete this->rightLayout;
	if (this->gameBoard != NULL)
		delete this->gameBoard;
	this->rightLayout = new QVBoxLayout;
	this->gameBoard = new GameBoard(this);
	QLabel *timeLabel = new QLabel(tr("Time"));
	QLineEdit *time = new QLineEdit(tr("0:00:00"));
	rightLayout->addWidget(timeLabel);
	rightLayout->addWidget(time);
	mainLayout->addWidget(gameBoard);
	mainLayout->addLayout(rightLayout);
	Trapeze *tempTrapeze;
	/*for (int i = 1; i <= 9; i++){
		tempTrapeze = new Trapeze(this, i);
		rightLayout->addWidget(tempTrapeze);
	}*/

}

void Sudoku::newGameSlot(){
	newGame();
}