﻿var bColumns = 9;
var bRows = 9;
var offset = 100;
var bPieceWidth =50;
var bPieceHeight = 50;
var bPixelWidth = 1 + (bColumns * bPieceWidth) + offset * 2;
var bPixelHeight = 1 + (bRows * bPieceHeight) + offset * 2;

var exteriorRadius = 100;
var interiorRadius = (bPieceWidth / 2) + 10;

var gCanvasElement;
var gDrawingContext;
var textCanvas;

var selectedCell;
var lastValue = 0;

var mouseIsDown = 0;
var mouse = { x: 0, y: 0 };
var gPattern = new Array();
var cellArray = new Array(81);

function Cell(row, column, value) {
    this.row = row;
    this.column = column;
    if(value > 0)
        this.original = true;
    else
        this.original = false
    this.number = value;
    this.hint = new Array();
}

function writeMessage(canvas, message) {
    var context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.font = '18pt Calibri';
    context.fillStyle = 'black';
    context.fillText(message, 10, 25);
}

function getPosition(x, y) {
    x = Math.min(x, bColumns * bPieceWidth + offset);
    y = Math.min(y, bRows * bPieceHeight + offset);
    y = Math.floor(y / bPieceHeight) - offset / bPieceHeight;
    x = Math.floor(x / bPieceWidth) - offset / bPieceWidth;
    var cell
    if (x >=0 && x <= 9 && y >= 0 && y < 9)
        cell = cellArray[9 * y + x];
    else
        cell = null;
    return cell;
}

function make2Dmap(exterior, interior) {
    var nonagon = new Array(9);
    var con = Math.PI / 180 * 40;
    for (var i = 0; i <= 9; i++) {
        nonagon[i] = i * con;
    }
    var init = exterior * (-1);
    for (var y = init; y < exterior; y++) {
        gPattern[y] = new Array();
        for (var x = init; x < exterior; x++) {
            var distance = Math.sqrt((x * x) + (y * y));
            var angle = Math.atan2(y, x);
            if (angle < 0) 
                angle += 2 * Math.PI;
            for (var i = 0; i < 9; i++) {
                if (angle >= nonagon[i] && angle < nonagon[i + 1]) {
                    if (distance <= exterior && distance >= interior)
                        gPattern[y][x] = i + 1;
                    else
                        gPattern[y][x] = 0;
                    break;
                }
            }
        }
    }
}

function drawCircle() {
    var x = (selectedCell.column * bPieceWidth) + (bPieceWidth / 2) + offset;
    var y = (selectedCell.row * bPieceHeight) + (bPieceHeight / 2) + offset;
    
    gDrawingContext.save();
    gDrawingContext.beginPath();
    gDrawingContext.arc(x, y, exteriorRadius, 0, Math.PI * 2, false);
    gDrawingContext.fillStyle = "rgb(208, 208, 208)";
    gDrawingContext.fill();
    gDrawingContext.clip()

    var n = 9;
    var eq = 360 / n;
    var radConst = Math.PI / 180;

    for (var i = 0; i <9; i++) {
        var rad = radConst * i * eq;

        gDrawingContext.beginPath();
        var x0 = interiorRadius * Math.cos(rad);
        var y0 = interiorRadius * Math.sin(rad);
        gDrawingContext.moveTo(x+x0, y+y0);
        var x1 = exteriorRadius * Math.cos(rad);
        var y1 = exteriorRadius * Math.sin(rad);


         if (selectedCell.number == i + 1) {
            // vybarvi segment podle zvoleneho cisla
            gDrawingContext.arc(x, y, exteriorRadius, rad, rad + 0.6981, false);
            var radNew = radConst * (i + 1) % 9 * eq;
            var x2 = interiorRadius * Math.cos(radNew);
            var y2 = interiorRadius * Math.sin(radNew);
            gDrawingContext.lineTo(x + x2, y + y2);
            gDrawingContext.fillStyle = "#00CC00";
            gDrawingContext.fill();
        } else if (lastValue == i + 1) {
            // vybarvi segment podle zvoleneho cisla
            gDrawingContext.arc(x, y, exteriorRadius, rad, rad + 0.6981, false);
            var radNew = radConst * (i + 1) % 9 * eq;
            var x2 = interiorRadius * Math.cos(radNew);
            var y2 = interiorRadius * Math.sin(radNew);
            gDrawingContext.lineTo(x + x2, y + y2);
            gDrawingContext.fillStyle = "#0000DD";
            gDrawingContext.fill();
        } else if (selectedCell.number == 0 && selectedCell.hint[i] != null && selectedCell.hint[i] != "") {
            // vybarvi segmenty napovedy
            gDrawingContext.arc(x, y, exteriorRadius, rad, rad + 0.6981, false);

            var radNew = radConst * (i + 1) % 9 * eq;
            var x2 = interiorRadius * Math.cos(radNew);
            var y2 = interiorRadius * Math.sin(radNew);
            gDrawingContext.lineTo(x + x2, y + y2);
            gDrawingContext.fillStyle = "#CC0000";
            gDrawingContext.fill();
        } else {
            gDrawingContext.lineTo(x + x1, y + y1);
        }


        gDrawingContext.stroke();
        gDrawingContext.closePath();

        gDrawingContext.fillStyle = "#000";
        gDrawingContext.fillText(i + 1, x + ((exteriorRadius - interiorRadius) * Math.cos(rad+0.34)), y + ((exteriorRadius - interiorRadius) * Math.sin(rad+0.34)));
    }

    gDrawingContext.beginPath();
    gDrawingContext.moveTo(x + (interiorRadius), y);
    gDrawingContext.arc(x, y, interiorRadius, 0, Math.PI * 2, false);
    gDrawingContext.fillStyle = "#FFF";
    gDrawingContext.fill();

    gDrawingContext.restore();
    gDrawingContext.arc(x, y, exteriorRadius, 0, Math.PI * 2, false);
    gDrawingContext.strokeStyle = "#555";
    gDrawingContext.stroke();
    gDrawingContext.closePath();

}


function drawBoard() {

    // vykresli heraci plochu
    gDrawingContext.clearRect(0, 0, bPixelWidth, bPixelHeight);
    gDrawingContext.beginPath();
    // vertical lines
    for (var x = offset; x <= bPixelWidth - offset; x += bPieceWidth) {
        gDrawingContext.moveTo(0.5 + x, offset);
        gDrawingContext.lineTo(0.5 + x, bPixelHeight - offset);
    }
    // horizontal lines
    for (var y = offset; y <= bPixelHeight - offset; y += bPieceHeight) {
        gDrawingContext.moveTo(offset, 0.5 + y);
        gDrawingContext.lineTo(bPixelWidth - offset, 0.5 + y)
    }
    gDrawingContext.strokeStyle = "#ccc";
    gDrawingContext.stroke();


    // Zvyrazni kazdou treti linku
    gDrawingContext.beginPath();
    // vertical lines
    for (var x = offset; x <= bPixelWidth - offset; x += bPieceWidth * 3) {
        gDrawingContext.moveTo(0.5 + x, offset);
        gDrawingContext.lineTo(0.5 + x, bPixelHeight - offset);
    }
    // horizontal lines
    for (var y = offset; y <= bPixelHeight - offset; y += bPieceHeight * 3) {
        gDrawingContext.moveTo(offset, 0.5 + y);
        gDrawingContext.lineTo(bPixelWidth - offset, 0.5 + y)
    }
    gDrawingContext.strokeStyle = "#000";
    gDrawingContext.stroke();

    // Vykresleni cisel
    var hintContext = gDrawingContext;
    gDrawingContext.beginPath();
    gDrawingContext.font = "18pt Calibri";
    gDrawingContext.textBaseline = "middle";
    gDrawingContext.textAlign = "center";
    var hint;
    for (var y = 0; y < 9; y++) {
        for (var x = 0; x < 9; x++) {

            if (cellArray[9 * y + x].number > 0) {
                if (cellArray[9 * y + x].original)
                    gDrawingContext.fillStyle = "#000";
                else
                    gDrawingContext.fillStyle = "#C00000";
                gDrawingContext.fillText(cellArray[9 * y + x].number, 50 * x + offset + bPieceWidth / 2, 50 * y + offset + bPieceHeight / 2);
            } else {
                hint = "";
                for (var i in cellArray[9 * y + x].hint) {
                    if (cellArray[9 * y + x].hint[i] != "")
                        hint += cellArray[9 * y + x].hint[i] + " ";
                }
                gDrawingContext.font = "8pt Calibri";
                hintContext.fillText(hint, 50 * x + offset + bPieceWidth / 2, 50 * y + offset + bPieceHeight / 2);
                gDrawingContext.font = "18pt Calibri";
            }
        }
    }
}

function numberSelect() {
    if (selectedCell != null) {
        x = mouse.x - ((selectedCell.column * bPieceWidth) + (bPieceWidth / 2) + offset);
        y = mouse.y - ((selectedCell.row * bPieceHeight) + (bPieceHeight / 2) + offset);
        var value;
        if (x >= -100 && x < 100 && y >= -100 && y < 100)
            value = gPattern[y][x];
        else
            value = null;

        if (value != null) {
            if (lastValue > 0 && value == 0) {
                if (selectedCell.hint[lastValue - 1] == lastValue)
                    selectedCell.hint[lastValue - 1] = "";
                else
                    selectedCell.hint[lastValue - 1] = lastValue;
            }
            lastValue = value;
            drawCircle();
            writeMessage(textCanvas, "mouse x: " + x + " y: " + y + " value: " + value);
        } else {
            mouseUp();
        }
    } 
}

function newGame() {
    // inicializuje cisla do hry
    var unsolved = "063700024090004036000503907006050208000209000702010300608902000120600070470001690";
    var   solved = "563798124897124536241563987916357248354289761782416359638972415129645873475831692";

    for(var y = 0; y<9; y++){
        for (var x = 0; x < 9; x++) {
            cellArray[9 * y + x] = new Cell(y, x, unsolved.charAt(9*y+x));
        }
    }

    drawBoard();
}

function mouseDown() {
    mouseIsDown = 1;
    selectedCell = getPosition(mouse.x, mouse.y);
    
    if (selectedCell != null && !selectedCell.original) {
        drawCircle();
    }
}

function touchStart() {
    touchXY();
    mouseIsDown = 1;
    selectedCell = getPosition(mouse.x, mouse.y);
    if (selectedCell != null && !selectedCell.original) {
        drawCircle();
    } 
}

function mouseUp() {
    mouseIsDown = 0;
    if (selectedCell != null && !selectedCell.original) {
        // set value
        if (lastValue == selectedCell.number)
            selectedCell.number = 0;
        else if (lastValue > 0) {
            selectedCell.number = lastValue;
            lastValue = 0;
        }
        lastValue = 0;
        selectedCell = null;
        drawBoard();
    }
}

function touchEnd() {
    mouseUp();
}

function mouseXY(e) {
    if (!e) var e = event;

    if (e.pageX != undefined && e.pageY != undefined) {
        mouse.x = e.pageX - gCanvasElement.offsetLeft;
        mouse.y = e.pageY - gCanvasElement.offsetTop;
    } else {
        mouse.x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        mouse.y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }


    if (mouseIsDown && selectedCell != null && !selectedCell.original) {
        numberSelect();
    }

}

function touchXY(e) {
    if (!e) var e = event;
    e.preventDefault();
    mouse.x = e.targetTouches[0].pageX - gCanvasElement.offsetLeft;
    mouse.y = e.targetTouches[0].pageY - gCanvasElement.offsetTop;

    if (mouseIsDown && !selectedCell.original) {
        numberSelect();
    }
}

function initGame(canvasElement) {
    if (!canvasElement) {
        canvasElement = document.createElement("canvas");
        canvasElement.id = "gameBoard";
        document.body.appendChild(canvasElement);
    } else {
        canvasElement = document.getElementById(canvasElement);
    }

    textCanvas = document.getElementById("textBox");

    gCanvasElement = canvasElement;
    gCanvasElement.onselectstart = function () { return false; }
    gCanvasElement.width = bPixelWidth;
    gCanvasElement.height = bPixelHeight;
    gCanvasElement.addEventListener('mousemove', mouseXY, false);
    gCanvasElement.addEventListener("mousedown", mouseDown, false);
    gCanvasElement.addEventListener("mouseup", mouseUp, false);
    gCanvasElement.addEventListener('touchmove', touchXY, true);
    gCanvasElement.addEventListener("touchstart", touchStart, false);
    gCanvasElement.addEventListener("touchend", touchEnd, false);
    gCanvasElement.addEventListener("touchcancel", touchEnd, false);
    gDrawingContext = gCanvasElement.getContext("2d");

    make2Dmap(exteriorRadius, interiorRadius);
    newGame();
    
}